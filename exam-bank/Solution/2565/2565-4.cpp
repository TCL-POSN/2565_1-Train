#include <stdio.h>
#include <math.h>
#include <string.h>
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
#define loops(n, m) for (int i = n; i < m; i++)
#define loops2(n, m) for (int j = n; j < m; j++)
#define loops3(n, m) for (int k = n; k < m; k++)
#define loopj(n, o) for (int i = 0; i < n; i+=o)
#define loopj2(n, o) for (int j = 0; j < n; j+=o)
#define loopj3(n, o) for (int k = 0; k < n; k+=o)
#define loopsj(n, m, o) for (int i = n; i < m; i+=o)
#define loopsj2(n, m, o) for (int j = n; j < m; j+=o)
#define loopsj3(n, m, o) for (int k = n; k < m; k+=o)
typedef long long ll;
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int isPrime(int num) {
    if (num%2==0) return 0;
    loopsj(3, num, 2) if (num%i==0) return 0;
    return 1;
}
int main() {
    long unsigned input; scanf("%d", &input);
    loops(2, input/2+2) if (input%i==0 && isPrime(i)==1 && isPrime(input/i)==1) {
        printf("%d\n%d", i, input/i);
        return 0;
    } printf("NO");
    return 0;
}
/***
 * Works up to 9 digits
 * 2022926781 = 1583 * 1277907
 ***/
