#include <stdio.h>
#include <math.h>
#include <string.h>
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
typedef long long ll;
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    int index = 1, reach, number = 3;
    getInt(&reach);
    while (index < reach) number += 2+index++;
    printf("%d", number);
    return 0;
}
