#include <stdio.h>
#include <math.h>
#include <string.h>
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
typedef long long ll;
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    char tile; int row, col, read;
    scanf("%c%1d%1d%1d", &tile, &row, &col, &read);
    /* char buffer[4], tile; getWord(buffer);
    int row = (int)buffer[1]-48,
        col = (int)buffer[2]-48,
        read = (int)buffer[3]-48;
    tile = buffer[0]; */
    if (tile >= 'a' && tile <= 'z') tile -= 32;
    tile += read-1;
    loop(col) {
        printf("%c", tile);
        tile += row;
        if (tile > 'Z') tile -= 26;
    }
    return 0;
}
