#include <stdio.h>
#include <math.h>
#include <string.h>
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
typedef long long ll;
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    int size, col, row; scanf("%1d%1d%1d", &size, &col, &row);
    /*char buffer[4]; getWord(buffer);
    int size = (int)buffer[0]-48,
        col = (int)buffer[1]-48,
        row = (int)buffer[2]-48;*/
    if (col<=1 || col>=size || row<=1 || row >=size)
        printf("NO");
    else {
        int surround = 0, temp;
        temp = (size*(row-1)-(size-col))%10;
        surround += temp; surround += (temp-1==0 ? 9 : temp-1); surround += (temp+1==10 ? 1 : temp+1);
        temp = (temp+size)%10 + (temp+size > 10 ? 1 : 0);
        surround += (temp-1==0 ? 9 : temp-1); surround += (temp+1==10 ? 1 : temp+1);
        temp = (size*(row+1)-(size-col))%10 + 1;
        surround += temp; surround += (temp-1==0 ? 9 : temp-1); surround += (temp+1==10 ? 1 : temp+1);
        printf("%d", surround);
    }
    return 0;
}
