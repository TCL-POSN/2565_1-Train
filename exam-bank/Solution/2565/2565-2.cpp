#include <stdio.h>
#include <math.h>
#include <string.h>
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
typedef long long ll;
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    char input[256]; getWord(input);
    int length = strlen(input), sum = 0;
    loop(length-1) {
        if (input[i]==input[i+1]) sum += (int)input[i]-48;
        else if (input[i]==input[i-1]) sum += (int)input[i]-48;
    } printf("%d", sum);
    return 0;
}
