#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    int numI, numR, numO[10] = {};
    getInt(&numI); getInt(&numR);
    for (int numT = numI; numT <= numR; numT++) {
        for (int numD = numT; numD>=1; numD = floor(numD/10)) numO[numD%10]++;
    } loop(10) printf("%d -> %d\n", i, numO[i]);
    return 0;
}
