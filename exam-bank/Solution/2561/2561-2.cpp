#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

#define WALL 20
int main() {
    // Without array
    int spike, height; getInt(&spike);
    loop(spike) {
        getInt(&height);
        loop2(height) { sp(WALL-j-1); printf("/\n"); }
        loop2(height) { sp(WALL-height+j); printf("\\\n"); }
    }
    /***
     * // With array
     * int spike; getInt(&spike);
     * int height[spike]; loop(spike) getInt(&height[i]);
     * loop(spike) {
     *     loop2(height[i]) { sp(WALL-j-1); printf("/\n"); }
     *     loop2(height[i]) { sp(WALL-height[i]+j); printf("\\\n"); }
     * }
     ***/
    return 0;
}
