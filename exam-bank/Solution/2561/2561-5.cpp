#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

#define MONTH 12
int leap = 0, dayStart = 1;
int getDay(int *m) {
    return (*m==2 ? (leap==0 ? 28 : 29) : 30 + (*m%2)*(*m>=8?-1:1)+(*m>=8?1:0));
}
int dayMonth(int *reach) {
    int remains = 0, month = 1;
    while (month != *reach) {
        remains += getDay(&month);
        if (++month > MONTH) month -= MONTH;
    } return (dayStart-1+remains)%7+1;
}
int main() {
    int dayMax, dayIter = 1, dayOfWeek, month;
    getInt(&dayStart); getInt(&leap); getInt(&month);
    dayStart = dayMonth(&month); dayMax = getDay(&month);
    printf("Sun Mon Tue Wed Thu Fri Sat\n");
    for (dayOfWeek = 0; dayOfWeek < dayStart-1; dayOfWeek++) printf("%-4s", " ");
    for (dayOfWeek = dayStart; dayOfWeek <= 7; dayOfWeek++) if (dayIter <= dayMax) printf("%-4d", dayIter++);
    while (dayIter <= dayMax) {
        printf("\n");
        for (dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++)
            if (dayIter <= dayMax) printf("%-4d", dayIter++);
    }
    return 0;
}
