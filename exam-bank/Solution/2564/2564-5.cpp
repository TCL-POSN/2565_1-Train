#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

#define fRow 3
const int fPat[6][fRow] = {
    {0, 1, 0},
    {0, 2, 0},
    {1, 1, 1},
    {2, 0, 2},
    {2, 1, 2},
    {2, 2, 2}
};
int main() {
    const int render = 3;
    int face[render]; scanf("%1d%1d%1d", &face[0], &face[1], &face[2]);
    if (face[0]<1 || face[0]>6 || face[1]<1 || face[1]>6 || face[2]<1 || face[2]>6) {
        printf("ERROR");
        return 0;
    } loop(fRow) {
        loop2(render) {
            if (face[j]==2) { if (i==1) printf(" * * "); else sp(5); } else
            switch (fPat[face[j]-1][i]) {
                case 0: sp(5); break;
                case 1: printf("  *  "); break;
                case 2: printf("*   *"); break;
                default: {}
            } if (j+1 < render) printf("|");
        } nl();
    }
    return 0;
}
