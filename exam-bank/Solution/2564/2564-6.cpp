#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

#define DIGITS 13
int main() {
    char barcode[DIGITS+1]; getWord(barcode);
    int sum = (int)barcode[DIGITS-1]-48;
    loop(DIGITS-1) sum += ((int)barcode[i]-48)*(i%2==1 ? 3 : 1);
    printf("%s", (sum%10==0 ? "YES" : "NO"));
    return 0;
}
