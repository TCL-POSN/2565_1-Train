#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

// Not Fibonacci nor Lucas number
int customSequence(int *pos, int *f1, int *f2, int *reach) {
    if (*pos%2==0) *f1 += *f2;
    else *f2 += *f1;
    if (++(*pos) < *reach) customSequence(pos, f1, f2, reach);
    return (*pos%2==0 ? *f1 : *f2);
}
int main() {
    int limit, f1 = 1, f2 = 1, pos = 1; getInt(&limit);
    printf("%d", customSequence(&pos, &f1, &f2, &limit));
    return 0;
}
