#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int prime(int *n) {
    if (*n%2==0) return 0;
    for (int j = 3; j < *n; j+=2) {
        if (*n%j==0) return 0;
    } printf(", %d", *n);
	return 1;
}
int main() {
    int q, c = 1, i = 3; getInt(&q);
    printf("2");
	if (q > 1) while(c < q) {
        c += prime(&i); i += 2;
    }
    return 0;
}
