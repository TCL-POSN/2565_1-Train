#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
#include <time.h>
#include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }
#include <algorithm>
int random(int start=0, int stop=1) { return rand()%(stop-start+1)+start; }

int main() {
    srand(time(NULL));
    int amount; getInt(&amount);
    int amtD = (int)ceil(amount*0.5), amtA = amount/2;
    int setD[amtD], setA[amtA];
    loop(amtD) {
        setD[i] = random(1, 99);
        printf("%-2d ", setD[i]);
    } printf("|"); loop(amtA) {
        setA[i] = random(1, 99);
        printf(" %-2d", setA[i]);
    } nl();
    std::sort(setD, setD+amtD);
    std::sort(setA, setA+amtA);
    loop(amtD) printf("%-2d ", setD[amtD-1-i]);
    printf("|");
    loop(amtA) printf(" %-2d", setA[i]);
    return 0;
}
