#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(char *q, int *to) {
    printf("%s : ", q);
    scanf("%d", to);
}
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    int height, lookup; getInt("h", &height);
    getInt("n", &lookup); getInt("c", &lookup);
    printf("%c", 65+(lookup-1)%height);
    loop(height-1) printf(", %c", 65+(lookup+i)%height);
    return 0;
}
