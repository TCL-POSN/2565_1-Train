#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    char input[64]; int table;
    gets(input); getInt(&table);
    int length = strlen(input), lookup = 0, col = 0;
    do {
        if (lookup < length) {
            printf("%c", input[lookup]==' ' ? '7' : input[lookup]);
            lookup += table;
        } else lookup = ++col;
    } while (col < table);
    return 0;
}
