#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

void prime(int *n) {
    if (*n%2==0) return;
    for (int i = 3; i < *n; i+=2) {
        if (*n%i==0) return;
    } printf("%d\n", *n);
}
int main() {
    int q, c = 2;
    getInt(&q); printf("2\n");
	while (c++ < q) prime(&c);
    return 0;
}
