#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

#define MONTH 12
int getDay(int *m) {
    return (*m==2 ? 28 : 30 + (*m%2)*(*m>=8?-1:1)+(*m>=8?1:0));
}
int main() {
    int month, day, remains = 0;
    getInt(&month); getInt(&day);
    while (month != 8) {
        remains += getDay(&month);
        if (++month > MONTH) month -= MONTH;
    } printf("%d", (day+3+remains)%7+1);
    return 0;
}
