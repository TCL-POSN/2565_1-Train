#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(char *q, int *to) {
    printf("%s", q);
    scanf("%d", to);
}
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

ll convertToDec(char numBase[], int base){
    ll numDec = 0; int digit = strlen(numBase);
    loop(digit) {
        int val = (int)numBase[digit-1-i];
        if (val>=48 && val<=57) val -= 48; // 0 = 48
        else if(val>=65 && val<=90) val -= 55; // 10 = A
        else if(val>=97 && val<=122) val -= 61; // 36 = a
        else if(val==43 || val==44) val += 19; // 62 = +
        numDec += val*pow(base, i);
        // printf("$~: %d*%d^%d\n", val, base, i);
    } return numDec;
}
char convertToBase(ll number, int base){
    if (number > 0) {
        int val = number%base;
        if (val>=0 && val<=9) val += 48; // 0 = 48
        else if(val>=10 && val<=35) val += 55; // 10 = A
        else if(val>=36 && val<=61) val += 61; // 36 = a
        else if(val=='+' || val=='/') val -= 19; // 62 = +
        printf("%c", convertToBase(number/base, base));
        return val;
    } else return '\t';
}
#define MAXLENGTH 3
int main() {
    int base; ll numDec; char numBase[MAXLENGTH];
    getInt("Base\t", &base); int base2 = base;
    printf("Num1\t"); scanf("%s", numBase); numDec = convertToDec(numBase, base2);
    printf("Num2\t"); scanf("%s", numBase); numDec += convertToDec(numBase, base2);
    printf("OUTPUT"); printf("%c", convertToBase(numDec, base2));
    return 0;
}
