#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    int base; getInt(&base); /* do {
        getInt("Size (1-99):", &base);
    } while (base < 1 || base > 99); */ int current = base-1;
    loop(base) {
        sp(2*i);
        printf("%c ", 65+current%26);
        if (current>0) {
            sp(4*(base-i)-6);
            printf("%c ", 65+current%26);
        } sp(4*i);
        printf("* ");
        if ((current--)>0) {
            sp(4*(base-i)-6);
            printf("*");
        } nl();
    } loop(base-1) {
        sp(2*(base-2-i));
        printf("%c ", 65+(++current)%26);
        sp(4*i+2);
        printf("%c ", 65+current%26);
        sp(4*(base-2-i));
        printf("* ");
        sp(4*i+2);
        printf("*\n");
    }
    return 0;
}
