#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
	int numA, numB; scanf("%x", &numA); scanf("%x", &numB);
	// printf("%x", numA+numB);
	char ans[8]; sprintf(ans, "%x", numA+numB);
	loop(strlen(ans)) {
		if (ans[i] >= 'a' && ans[i] <= 'z') ans[i] -= 32;
		printf("%c", ans[i]);
	}
    return 0;
}
