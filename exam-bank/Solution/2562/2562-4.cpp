#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int isPrime(int n) {
	if (n%2==0) return 0;
	for (int j = 3; j < n; j+= 2) {
		if (n%j==0) return 0;
	} return 1;
}
int main() {
	int test; getInt(&test);
	for (int i = 2; i <= ceil(test*0.5); i++) {
		if (test%i==0 && isPrime(i)+isPrime(test/i)==2) {
			printf("%d\n%d", i, test/i);
			return 0;
		}
	} printf("no");
    return 0;
}
