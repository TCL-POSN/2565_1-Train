#include <stdio.h>
#include <math.h>
#include <string.h>
// #include <conio.h>
// #include <ctype.h>
// #include <time.h>
// #include <stdlib.h>
typedef long long ll;
#define loop(n) for (int i = 0; i < n; i++)
#define loop2(n) for (int j = 0; j < n; j++)
#define loop3(n) for (int k = 0; k < n; k++)
void getWord(char ans[]) { scanf("%s", ans); }
void getInt(int *to) { scanf("%d", to); }
void sp(int spaces=1) { loop(spaces) printf(" "); }
void nl(int lines=1) { loop(lines) printf("\n"); }

int main() {
    int length, height, lookup; char tile = 'A';
    getInt(&length); getInt(&height); getInt(&lookup);
    for (int row = 0; row < height && --lookup>=0; row++) {
    	if (tile + lookup > 'Z') tile -= 26;
        printf("%c", tile+lookup);
        tile += length;
    }
    return 0;
}
